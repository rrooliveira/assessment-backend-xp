<?php

namespace App\Database;

use PDO;

class Database
{
    private $pdo;
    private $dbname = 'xp';
    private $host = 'mysql-server';
    private $user = 'root';
    private $pass = 'password';

    private function __construct()
    {
        try {
            $this->pdo = new PDO("mysql:dbname=" . $this->dbname . ";host=" . $this->host . ";", $this->user, $this->pass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public static function getInstance()
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new Database();
        }
        return $instance;
    }

    public function query($sql)
    {
        return $this->pdo->query($sql);
    }

    public function prepare($sql)
    {
        return $this->pdo->prepare($sql);
    }

    public function execute($array)
    {
        return $this->pdo->exec($array);
    }

    public function beginTransaction()
    {
        return $this->pdo->beginTransaction();
    }

    public function commit()
    {
        return $this->pdo->commit();
    }

    public function rollback()
    {
        return $this->pdo->rollBack();
    }

    public function lastInsertedId()
    {
        return $this->pdo->lastInsertId();
    }
}