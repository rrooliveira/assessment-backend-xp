<?php

namespace App\Utils;

use App\Interfaces\SanitizeInterface;

class SanitizeString implements SanitizeInterface
{
    public static function filter($value)
    {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }
}