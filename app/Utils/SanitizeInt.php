<?php

namespace App\Utils;

use App\Interfaces\SanitizeInterface;

class SanitizeInt implements SanitizeInterface
{
    public static function filter($value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }
}