<?php

namespace App\Utils;

use App\Interfaces\SanitizeInterface;

class SanitizeFloat implements SanitizeInterface
{
    public static function filter($value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }
}