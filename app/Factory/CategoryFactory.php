<?php

namespace App\Factory;

use App\Models\Category;

class CategoryFactory
{
    public static function create(string $name, string $code): Category
    {
        $objCategory = new Category();
        $objCategory->setName($name);
        $objCategory->setCode($code);

        return $objCategory;
    }
}