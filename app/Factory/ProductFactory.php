<?php

namespace App\Factory;

use App\Models\Product;

class ProductFactory
{
    public static function create(string $name, string $sku, float $price, string $description, int $quantity, array $category): Product
    {
        $objProduct = new Product();
        $objProduct->setName($name);
        $objProduct->setSku($sku);
        $objProduct->setPrice($price);
        $objProduct->setDescription($description);
        $objProduct->setQuantity($quantity);
        $objProduct->setCategory($category);

        return $objProduct;
    }
}