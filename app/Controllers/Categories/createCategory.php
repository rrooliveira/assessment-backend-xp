<?php
include_once '../../../vendor/autoload.php';

use App\Factory\CategoryFactory;
use App\Strategy\SanitizeStrategy;
use App\Utils\SanitizeString;

try {
    $name = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['name']);
    $code = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['code']);

    $objCategory = CategoryFactory::create($name, $code);
    $checked = $objCategory->checkFilledFields();

    if ($checked instanceof Exception) {
        throw $checked;
    }

    $categoryInserted = $objCategory->create();

    if($categoryInserted) {
        echo 'Categoria criada com sucesso.';
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
