<?php
include_once '../../../vendor/autoload.php';

use App\Factory\CategoryFactory;
use App\Strategy\SanitizeStrategy;
use App\Utils\SanitizeInt;
use App\Utils\SanitizeString;

try {
    $name = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['name']);
    $code = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['code']);
    $id = SanitizeStrategy::sanitize(new SanitizeInt(), $_POST['id']);

    $objCategory = CategoryFactory::create($name, $code);
    $checked = $objCategory->checkFilledFields();

    if ($checked instanceof Exception) {
        throw $checked;
    }

    $categoryUpdated = $objCategory->update($id);

    if($categoryUpdated) {
        echo 'Categoria alterada com sucesso.';
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
