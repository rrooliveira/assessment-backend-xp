<?php
include_once '../../../vendor/autoload.php';

use App\Models\Category;
use App\Strategy\SanitizeStrategy;
use App\Utils\SanitizeString;

try {
    $uuid = SanitizeStrategy::sanitize(new SanitizeString(), $_GET['p']);

    $objCategory = new Category();
    $categoryDeleted = $objCategory->delete($uuid);

    if ($categoryDeleted) {
        echo 'Categoria deletada com sucesso!';
    } else {
        echo 'Esta categoria possui produtos vinculados a ela e não pode ser removida.';
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
