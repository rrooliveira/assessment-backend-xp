<?php
include_once '../../../vendor/autoload.php';

use App\Factory\ProductFactory;
use App\Models\Category;
use App\Models\Product;
use App\Strategy\SanitizeStrategy;
use App\Utils\SanitizeFloat;
use App\Utils\SanitizeInt;
use App\Utils\SanitizeString;

try {
    $name = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['name']);
    $sku = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['sku']);
    $price = SanitizeStrategy::sanitize(new SanitizeFloat(), $_POST['price']);
    $description = SanitizeStrategy::sanitize(new SanitizeString(), $_POST['description']);
    $quantity = SanitizeStrategy::sanitize(new SanitizeInt(), $_POST['quantity']);
    $category = [];

    $objCategory = new Category();

    if (is_array($_POST['category'])) {
        foreach ($_POST['category'] as $value){
            $idCategory = SanitizeStrategy::sanitize(new SanitizeInt(), $value);

            $arrayCategory = $objCategory->validateCategory($idCategory);

            if (!empty($arrayCategory)) {
                $category[] = $arrayCategory['id'];
            } else {
                throw new Exception('Categoria inválida!');
            }
        }
    } else {
        throw new Exception('Erro ao obter os dados da categoria.');
    }

    $objProduct = ProductFactory::create($name, $sku, $price, $description, $quantity, $category);
    $checked = $objProduct->checkFilledFields();

    if ($checked instanceof Exception) {
        throw $checked;
    }

    $productInserted = $objProduct->create();

    if ($productInserted) {
        echo 'Produto cadastrado com sucesso!';
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
