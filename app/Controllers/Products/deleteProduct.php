<?php
include_once '../../../vendor/autoload.php';

use App\Models\Product;
use App\Strategy\SanitizeStrategy;
use App\Utils\SanitizeString;

try {
    $uuid = SanitizeStrategy::sanitize(new SanitizeString(), $_GET['p']);

    $objProduct = new Product();
    $productDeleted = $objProduct->delete($uuid);

    if ($productDeleted) {
        echo 'Produto deletado com sucesso!';
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
