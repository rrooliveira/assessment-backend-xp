<?php

namespace App\Models;

use App\Interfaces\ProductInterface;
use App\Database\Database;
use PDOException;
use Ramsey\Uuid\Uuid;

class Product implements ProductInterface
{
    private $db;
    private $name;
    private $sku;
    private $price;
    private $description;
    private $quantity;
    private $category;
    private $uuid;
    protected $fillable = ['name', 'sku', 'price', 'description', 'quantity', 'category'];

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    public function create()
    {
        try {
            $sql = $this->db->prepare("INSERT INTO products (name, sku, price, quantity, description, uuid) values (:name, :sku, :price, :quantity, :description, :uuid)");

            $this->db->beginTransaction();

            $sql->bindValue(':name', $this->name);
            $sql->bindValue(':sku', $this->sku);
            $sql->bindValue(':price', $this->price);
            $sql->bindValue(':quantity', $this->quantity);
            $sql->bindValue(':description', $this->description);
            $sql->bindValue(':uuid', Uuid::uuid4());

            $sql->execute();

            $lastId = $this->db->lastInsertedId();
            if ((int)$lastId > 0) {
                $objProdCat = new ProductsCategories();

                foreach ($this->category as $category) {
                    $objProdCat->setIdProduct($lastId);
                    $objProdCat->setIdCategory($category);

                    $productCreated = $objProdCat->create();

                    if ($productCreated instanceof PDOException) {
                        throw $productCreated;
                    }
                }
            }

            $this->db->commit();

            return true;
        } catch (PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function update($idProduct)
    {
        try {
            $sql = $this->db->prepare("UPDATE products SET name = :name, 
                                                                sku = :sku, 
                                                                price = :price, 
                                                                quantity = :quantity, 
                                                                description = :description
                                               WHERE id = :id");

            $this->db->beginTransaction();

            $sql->bindValue(':name', $this->name);
            $sql->bindValue(':sku', $this->sku);
            $sql->bindValue(':price', $this->price);
            $sql->bindValue(':quantity', $this->quantity);
            $sql->bindValue(':description', $this->description);
            $sql->bindValue(':id', $idProduct);

            $sql->execute();

            $updated = $sql->rowCount();

            $objProdCat = new ProductsCategories();
            $deleted = $objProdCat->deleleRelationByProduct($idProduct);

            if ($deleted) {
                foreach ($this->category as $category) {

                    $objProdCat->setIdProduct($idProduct);
                    $objProdCat->setIdCategory($category);
                    $productCreated = $objProdCat->create();

                    if ($productCreated instanceof PDOException) {
                        throw $productCreated;
                    }
                }
            }

            $this->db->commit();

            return true;
        } catch (PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function delete($uuid)
    {
        try {
            $this->db->beginTransaction();

            $sql = $this->db->prepare("UPDATE products SET deleted = '*' WHERE uuid = :uuid");
            $sql->bindValue(':uuid', $uuid);
            $sql->execute();

            $this->db->commit();

            return true;
        } catch (PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllProducts()
    {
        $arrayProducts = [];
        $sql = $this->db->query('SELECT * FROM products WHERE deleted IS NULL');

        if ($sql->rowCount() > 0) {
            $arrayProducts = $sql->fetchAll();
        }

        if (!empty($arrayProducts)) {

            foreach ($arrayProducts as $key => $product) {
                $objProdCat = new ProductsCategories();
                $arrayCategories = $objProdCat->getCategoriesByProducts($product['id']);

                if (!empty($arrayCategories)) {
                    foreach ($arrayCategories as $category) {
                        $arrayProducts[$key]['category'][] = $category['name'];
                    }
                }
            }
        }

        return $arrayProducts;
    }

    public function getProductByUuid($uuid): array
    {
        try {
            $array = [];
            $sql = $this->db->prepare('SELECT * FROM products WHERE uuid = :uuid');
            $sql->bindValue(':uuid', $uuid);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetch();
            }

            $this->getCategoriesByProduct($array['id']);

            if (!empty($this->category)) {
                $array['category'] = $this->category;
            }

            return $array;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function checkFilledFields()
    {
        try {
            foreach ($this->fillable as $field) {
                if (empty($this->{$field})) {
                    throw new \DomainException('É necessário informar um valor para o campo ' . $field);
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    private function getCategoriesByProduct($idProduct): void
    {
        try {
            $objProdCat = new ProductsCategories();
            $arrayCategories = $objProdCat->getIdCategoriesByProducts($idProduct);

            if (!empty($arrayCategories)) {
                foreach ($arrayCategories as $category) {
                    $this->category[] = $category['id_category'];
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getQuantityProductsActive()
    {
        try {
            $quantity = 0;
            $sql = $this->db->prepare('SELECT COUNT(id) as quantity FROM products WHERE deleted IS NULL');
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetch();
                $quantity = $array['quantity'];
            }
            return $quantity;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}