<?php

namespace App\Models;

use App\Database\Database;

class ProductsCategories
{
    private $db;
    private $idProduct;
    private $idCategory;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getIdProduct()
    {
        return $this->idProduct;
    }

    public function setIdProduct($idProduct): void
    {
        $this->idProduct = $idProduct;
    }

    public function getIdCategory()
    {
        return $this->idCategory;
    }

    public function setIdCategory($idCategory): void
    {
        $this->idCategory = $idCategory;
    }

    public function create()
    {
        try {
            $sql = $this->db->prepare("INSERT INTO products_categories (id_product, id_category) values (:id_product, :id_category)");
            $sql->execute([
                'id_product' => $this->idProduct,
                'id_category' => $this->idCategory,
            ]);

            return $this->db->lastInsertedId();
        } catch (PDOException $e) {
            echo $e;
        }
    }

    public function getCategoriesByProducts($idProduct)
    {
        try {
            $array = [];
            $sql = $this->db->prepare('SELECT * FROM products_categories AS pc 
                                            JOIN categories as c 
                                            ON c.id = pc.id_category
                                            WHERE pc.id_product = :id_product
                                            AND pc.deleted IS NULL');
            $sql->bindValue(':id_product', $idProduct);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetchAll();
            }

            return $array;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    public function getIdCategoriesByProducts($idProduct)
    {
        try {
            $array = [];
            $sql = $this->db->prepare('SELECT id_category FROM products_categories
                                            WHERE id_product = :id_product
                                            AND deleted IS NULL');
            $sql->bindValue(':id_product', $idProduct);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetchAll();
            }

            return $array;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    public function deleleRelationByProduct($idProduct)
    {
        try {
            $sql = $this->db->prepare('UPDATE products_categories SET deleted = \'*\' WHERE id_product = :id_product');

            $sql->bindValue(':id_product', $idProduct);
            $sql->execute();

            $deleted = $sql->rowCount();

            return $deleted;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            return $e;
        }
    }

    public function getProductsByCategories($idCategory)
    {
        try {
            $array = [];
            $sql = $this->db->prepare('SELECT * FROM products_categories AS pc 
                                            JOIN categories as c 
                                            ON c.id = pc.id_category
                                            WHERE pc.id_category = :id_category
                                            AND pc.deleted IS NULL');
            $sql->bindValue(':id_category', $idCategory);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetchAll();
            }

            return $array;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            return $e;
        }
    }
}