<?php

namespace App\Models;

use App\Database\Database;
use App\Interfaces\CategoryInterface;
use Ramsey\Uuid\Uuid;

class Category implements CategoryInterface
{
    private $db;
    private $name;
    private $code;
    private $uuid;

    protected $fillable = ['name', 'code'];

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    public function create()
    {
        try {
            $this->db->beginTransaction();

            $sql = $this->db->prepare("INSERT INTO categories (name, code, uuid) values (:name, :code, :uuid)");

            $sql->bindValue(':name', $this->name);
            $sql->bindValue(':code', $this->code);
            $sql->bindValue(':uuid', Uuid::uuid4());

            $sql->execute();

            $lastId = $this->db->lastInsertedId();
            if ((int)$lastId > 0) {
                $this->db->commit();
            }

            return true;

        } catch (PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function update($idCategory)
    {
        try {
            $this->db->beginTransaction();

            $sql = $this->db->prepare("UPDATE categories SET name = :name, 
                                                                code = :code
                                               WHERE id = :id");

            $sql->bindValue(':name', $this->name);
            $sql->bindValue(':code', $this->code);
            $sql->bindValue(':id',$idCategory);

            $sql->execute();

            $this->db->commit();

            return true;
        } catch (\PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function delete($uuid)
    {
        try {

            $arrayProducts = $this->checkIfHasProductInCategory($uuid);

            if(!empty($arrayProducts)) {
                return false;
            }

            $this->db->beginTransaction();

            $sql = $this->db->prepare("UPDATE categories SET deleted = '*' WHERE uuid = :uuid");
            $sql->bindValue(':uuid', $uuid);
            $sql->execute();

            $this->db->commit();

            return true;
        } catch (PDOException $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
    }

    public function checkFilledFields()
    {
        try {
            foreach ($this->fillable as $field) {
                if (empty($this->{$field})) {
                    throw new \Exception('É necessário informar um valor para o campo ' . $field);
                }
            }

            return true;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function validateCategory($idCategory)
    {
        try {
            $arrayCategory = [];

            $sql = $this->db->prepare('SELECT * FROM categories WHERE id = :id');
            $sql->bindValue(':id', $idCategory);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $arrayCategory = $sql->fetch();
            }
            return $arrayCategory;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getAllCategories()
    {
        try {
            $arrayCategory = [];

            $sql = $this->db->query('SELECT * FROM categories WHERE deleted IS NULL');
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $arrayCategory = $sql->fetchAll();
            }

            return $arrayCategory;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getCategoryByUuid($uuid)
    {
        try {
            $arrayCategory = [];

            $sql = $this->db->prepare('SELECT * FROM categories WHERE uuid = :uuid AND deleted IS NULL');
            $sql->bindValue(':uuid', $uuid);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $arrayCategory = $sql->fetch();
            }

            return $arrayCategory;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function checkIfHasProductInCategory($uuid)
    {
        try {
            $arrayProducts = [];

            $sql = $this->db->prepare('SELECT * FROM categories WHERE uuid = :uuid AND deleted IS NULL');
            $sql->bindValue(':uuid', $uuid);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $arrayCategory = $sql->fetch();
            }

            if (!empty($arrayCategory)) {
                $objProdCat = new ProductsCategories();
                $arrayProducts = $objProdCat->getProductsByCategories($arrayCategory['id']);
            }

            return $arrayProducts;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
}