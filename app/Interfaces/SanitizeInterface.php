<?php

namespace App\Interfaces;

interface SanitizeInterface
{
    public static function filter($value);
}