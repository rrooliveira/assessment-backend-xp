<?php

namespace App\Strategy;

use App\Interfaces\SanitizeInterface;

class SanitizeStrategy
{
    public static function sanitize(SanitizeInterface $sanitize, $value){
        return $sanitize->filter($value);
    }
}