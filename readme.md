# Estrutura do Projeto

- PHP 7.3
- MySQL 5.7
- Design Patterns (Factory, Strategy e Singleton)
- Docker

# Executar com o Docker
- Na pasta do projeto e executar o comando `docker-compose up -d` 
- Url do projeto: http://localhost:8088
- Os containers já possuem os arquivos do projeto e o banco de dados estruturado
- Após a criação dos container, executar o comando `composer update` e `composer dumpautoload`

# Executar separadamente
- Na pasta mysql/script, tem um arquivo para criação do banco de dados, caso seja necessário
- Usuário do banco de dados `root` e a senha `password`