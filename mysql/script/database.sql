-- Copiando estrutura do banco de dados para xp
CREATE DATABASE IF NOT EXISTS `xp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `xp`;

-- Copiando estrutura para tabela xp.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` varchar(50) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Table of categories';

-- Copiando dados para a tabela xp.categories: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `code`, `uuid`, `deleted`) VALUES
	(00000000001, 'Comedy', 'CM', 'd317b60c-64c2-46e9-95c6-02201e7d3c8d', NULL),
	(00000000002, 'Horror', 'HR', '0236fda3-ce1b-46d1-a19b-0711e31acb13', NULL),
	(00000000003, 'Thriller', 'TH', '64e73b0b-1e0c-4c26-9eff-4bd5e04329d3', NULL),
	(00000000004, 'Documentary', 'DC', '411b9803-16ca-4c9e-a3bc-f7c4ce7b9918', NULL),
	(00000000005, 'Adventure', 'AD', '00c8fff5-0995-4a6a-bd13-462892b38d76', NULL),
	(00000000006, 'Animation', 'AN', '2364c9d7-11e5-4d33-b2d6-c352b424c8af', NULL),
	(00000000007, 'Children', 'CH', '817f37ff-0e24-4b19-818c-2e6e2a0c7728', NULL),
	(00000000008, 'Fantasy', 'FT', 'b09ec136-c192-446e-8ae1-a70327389cbe', NULL),
	(00000000009, 'Crime', 'CR', '047be73f-9a34-4e73-923e-0c3718a69c83', NULL),
	(00000000010, 'Mystery', 'MS', 'e5c0bc18-1833-4c8f-b5b0-3fd359cb732a', NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Copiando estrutura para tabela xp.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `sku` varchar(150) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Table of products';

-- Copiando dados para a tabela xp.products: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `sku`, `price`, `quantity`, `description`, `uuid`, `deleted`) VALUES
	(00000000013, 'Eplerenone', '42254-011', 2300.39, 59, 'Bypass Innominate Artery to Bilateral Upper Leg Artery with Synthetic Substitute, Open Approach', '042cb0af-a425-4d03-8570-92bc52ebc692', NULL),
	(00000000014, 'Covergirl Queen Collection All Day Flawless 3in1 Foundation', '22700-149', 3657.53, 38, 'Resection of Left Lower Arm and Wrist Tendon, Open Approach', '3f0c1b0b-d7c1-4ae2-80ee-3c83c580f148', NULL),
	(00000000015, 'Clonidine Hydrochloride', '0517-0730', 4866.35, 50, 'Restriction of Splenic Vein with Extraluminal Device, Open Approach', '7e347225-c797-4998-b015-a7ceaf06a435', NULL),
	(00000000016, 'Intelence', '59676-572', 3172.88, 38, 'Drainage of Right Foot Muscle, Percutaneous Approach, Diagnostic', '9d205f63-81df-421d-b8e5-81fedbe5b09a', NULL),
	(00000000017, 'Chorionic Gonadotropin', '63323-025', 4042.02, 97, 'Division of Left Lower Leg Muscle, Percutaneous Endoscopic Approach', '058fb8bc-123e-40cf-805e-0ef41bee427a', NULL),
	(00000000018, 'acid controller', '59779-194', 600.23, 36, 'Introduction of Blood Brain Barrier Disruption Substance into Peripheral Artery, Open Approach', '682cdd40-6e23-4759-9abe-17705e71cd2f', NULL),
	(00000000019, 'Oral-B NeutraCare Mint', '0041-0241', 1748.27, 26, 'Low Dose Rate (LDR) Brachytherapy of Diaphragm using Other Isotope', 'aba0210d-d918-4a14-ace3-de9ab4654bf0', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Copiando estrutura para tabela xp.products_categories
CREATE TABLE IF NOT EXISTS `products_categories` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned zerofill NOT NULL,
  `id_category` int(11) unsigned zerofill NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products` (`id_product`),
  KEY `fk_categories` (`id_category`),
  CONSTRAINT `fk_categories` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`),
  CONSTRAINT `fk_products` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Table of join products x categories';

-- Copiando dados para a tabela xp.products_categories: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `products_categories` DISABLE KEYS */;
INSERT INTO `products_categories` (`id`, `id_product`, `id_category`, `deleted`) VALUES
	(00000000001, 00000000013, 00000000001, NULL),
	(00000000002, 00000000013, 00000000002, NULL),
	(00000000003, 00000000013, 00000000003, NULL),
	(00000000005, 00000000014, 00000000004, NULL),
	(00000000006, 00000000015, 00000000004, NULL),
	(00000000007, 00000000016, 00000000001, NULL),
	(00000000008, 00000000017, 00000000002, NULL),
	(00000000009, 00000000018, 00000000006, NULL),
	(00000000010, 00000000018, 00000000007, NULL),
	(00000000011, 00000000018, 00000000008, NULL),
	(00000000012, 00000000019, 00000000005, NULL);
/*!40000 ALTER TABLE `products_categories` ENABLE KEYS */;
